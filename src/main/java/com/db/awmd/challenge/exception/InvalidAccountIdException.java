package com.db.awmd.challenge.exception;

public class InvalidAccountIdException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2445473822322655136L;

	public InvalidAccountIdException(String message) {
	    super(message);
	}
}
