package com.db.awmd.challenge.exception;

public class NotEnoughAssetsException extends RuntimeException {

  /**
	 * 
	 */
	private static final long serialVersionUID = 3246347046616376318L;

    public NotEnoughAssetsException(String message) {
        super(message);
    }
}
