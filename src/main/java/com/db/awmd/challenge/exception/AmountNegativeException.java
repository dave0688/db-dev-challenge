package com.db.awmd.challenge.exception;

public class AmountNegativeException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2028315116812541549L;

	public AmountNegativeException(String message) {
        super(message);
    }
}
