package com.db.awmd.challenge.dto;

import java.math.BigDecimal;

public class AccountTransferDetailsDTO {
	
	/**
	 * Empty constructor, needed for Jackson
	 * 
	 * @author David Kreutzer
	 */
	public AccountTransferDetailsDTO() {	
		
	}
	
	private BigDecimal amount;
	private String accountTo;
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getAccountTo() {
		return accountTo;
	}
	public void setAccountTo(String accountTo) {
		this.accountTo = accountTo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountTo == null) ? 0 : accountTo.hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountTransferDetailsDTO other = (AccountTransferDetailsDTO) obj;
		if (accountTo == null) {
			if (other.accountTo != null)
				return false;
		} else if (!accountTo.equals(other.accountTo))
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AccountTransferDetailsDTO [amount=" + amount + ", accountTo=" + accountTo + "]";
	}	
}
