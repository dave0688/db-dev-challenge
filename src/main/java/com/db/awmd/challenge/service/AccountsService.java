package com.db.awmd.challenge.service;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.AmountNegativeException;
import com.db.awmd.challenge.exception.InvalidAccountIdException;
import com.db.awmd.challenge.exception.NotEnoughAssetsException;
import com.db.awmd.challenge.repository.AccountsRepository;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountsService {
	
  @SuppressWarnings("unused")
  private final Logger log = LoggerFactory.getLogger(AccountsService.class);

  @Getter
  private final AccountsRepository accountsRepository;
  
  @Autowired
  private final EmailNotificationService emailNotificationService;

  @Autowired
  public AccountsService(AccountsRepository accountsRepository,
		  				 EmailNotificationService emailNotificationService) {
    this.accountsRepository = accountsRepository;
    this.emailNotificationService = emailNotificationService;
  }

  public void createAccount(Account account) {
    this.accountsRepository.createAccount(account);
  }

  public Account getAccount(String accountId) {
    return this.accountsRepository.getAccount(accountId);
  }
  
  public Account updateAccount(Account account) {
	    return this.accountsRepository.updateAccount(account);
  }

  public AccountsRepository getAccountsRepository() {
	return this.accountsRepository;
  }

  /**
   * Transfers the {amount} from one account {accountFrom} to {acountTo}
   * 
   * @param accountFromId - Account ID of the source account 
   * @param accountToId - Account ID of the target account 
   * @param amount - amount of the transaction
   * 
   * @author David Kreutzer
   * @date 15.08.2017
   */
  public void transferBalance(String accountFromId, String accountToId, BigDecimal amount) 
		  throws InvalidAccountIdException, AmountNegativeException, NotEnoughAssetsException
  {
	
	  // Validity checks: Check if transfer is possible
	  
	  // TODO: Implement additional security check if the accountFrom is really from the user which sends the request. 
	  // Could be validated by getting the current user out of the spring security context.

	  Account accountFrom = this.getAccount(accountFromId);
	  Account accountTo = this.getAccount(accountToId);
	  
	  // Check if both accounts exist. If not, throw exception
	  if(accountFrom == null || accountTo == null) {
		  String nonExistentAccountIds = "";
		  if(accountFrom == null) {
			  nonExistentAccountIds += accountFromId;
		  }
		  if(accountTo == null) {
			  if(!"".equals(nonExistentAccountIds))
			  {
				  nonExistentAccountIds += ", ";
			  }
			  nonExistentAccountIds += accountFromId;
		  }		  
		  
		  throw new InvalidAccountIdException("The account/s " + nonExistentAccountIds + " could not be found in DB.");
	  }
	  
	  // Check if amount is positive. Exclude zero as we,, since it makes no sense to transfer Zero.
	  if(amount.compareTo(BigDecimal.ZERO) < 0) {
		  throw new AmountNegativeException("A negative (or 0) amount cannot be transferred");
	  }
	  
	  // Check if the source account has enough assets
	  if(accountFrom.getBalance().compareTo(amount) < 0) {
		  throw new NotEnoughAssetsException("Account " + accountFrom.getAccountId() + " has not enough assets to fulfill the transfer.");
	  }
	  
	  // if all is valid, start transfer
	  // First, reduce source account
	  accountFrom.setBalance(accountFrom.getBalance().subtract(amount));	  
	  this.getAccountsRepository().updateAccount(accountFrom);
	  
	  // Add new balance to account.
	  accountTo.setBalance(accountTo.getBalance().add(amount));
	  this.getAccountsRepository().updateAccount(accountTo);
	  
	  // TODO: Log transaction somehow 
	  // (i.e. in a table with ID, accountFrom, accountTo, amount, dateExecuted, type (i.e. withdraw or deposit)
	  	  
	  // Send notification to both participants about the transfer
	  this.emailNotificationService.notifyAboutTransfer(accountTo, "The amount of " + amount.toString() + " has been transferred to your account from account " + accountFrom.getAccountId() + " at " + ZonedDateTime.now());
	  this.emailNotificationService.notifyAboutTransfer(accountFrom, "The amount of " + amount.toString() + " has been transferred from your account to account " + accountTo.getAccountId() + " at " + ZonedDateTime.now());
  }
}








