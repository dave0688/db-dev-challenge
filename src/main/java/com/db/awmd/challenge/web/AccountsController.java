package com.db.awmd.challenge.web;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.dto.AccountTransferDetailsDTO;
import com.db.awmd.challenge.exception.AmountNegativeException;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import com.db.awmd.challenge.exception.InvalidAccountIdException;
import com.db.awmd.challenge.exception.NotEnoughAssetsException;
import com.db.awmd.challenge.service.AccountsService;

import java.math.BigDecimal;

import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/accounts")
@Slf4j
public class AccountsController {

  private final Logger log = LoggerFactory.getLogger(AccountsController.class);

  private final AccountsService accountsService;

  @Autowired
  public AccountsController(AccountsService accountsService) {
    this.accountsService = accountsService;
  }

  @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> createAccount(@RequestBody @Valid Account account) {
    log.info("Creating account {}", account);

    try {
    	this.accountsService.createAccount(account);
    } catch (DuplicateAccountIdException daie) {
        return new ResponseEntity<>(daie.getMessage(), HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(HttpStatus.CREATED);
  }

  @GetMapping(path = "/{accountId}")
  public Account getAccount(@PathVariable String accountId) {
    log.info("Retrieving account for id {}", accountId);
    return this.accountsService.getAccount(accountId);
  }
  
  /**
   * PUT  /trnsferBalance : Transfer balance from account {accountFrom} to {accountTo}.
   * 
   * @param accountFrom - source account id
   * @param accountTo - target account id
   * @param amount - amount to transfer
   * 
   * @return ResponseEntity - true if the transfer was successful, false if not.
   * 
   * @author David Kreutzer
   * @date 15.08.2017
   */
  @PutMapping(path = "/{accountFrom}/transfer")
  public ResponseEntity<Object> transferBalance(@PathVariable("accountFrom") String accountFrom, @RequestBody AccountTransferDetailsDTO accountTransferDetails) {
	  
	  BigDecimal amount = accountTransferDetails.getAmount();
	  String accountTo = accountTransferDetails.getAccountTo();
	  
	  log.info("Starting transfer {} amount from account {} to account {}", amount, accountFrom, accountTo);  
	  
	  try {
		  this.accountsService.transferBalance(accountFrom, accountTo, amount);
	  } catch (InvalidAccountIdException|AmountNegativeException|NotEnoughAssetsException ex) {
		  
		  return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);		  
	  }

	  return new ResponseEntity<>(HttpStatus.OK);	  
  }

}
